#include <stdbool.h>
#include <stdint.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_delay.h"


#include "sensor_gpio.h"
#include "sensor_ble.h"
#include "sensor_timer.h"
#include "battery.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "fstorage.h"


#define DEAD_BEEF                       0xDEADBEEF			  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

uint8_t battery_level;


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void
assert_nrf_callback (uint16_t line_num, const uint8_t * p_file_name)
{
  app_error_handler (DEAD_BEEF, line_num, p_file_name);
}

void
handle_sensor ()
{
  nrf_drv_gpiote_out_set (PIN_OUT);
  disable_input ();
  if (!advertising_started)
    {
      battery_level = get_current_level ();
    }

  advertising_data_update (contact, interrupt_count, battery_level);
  advertising_start ();

  advertising_timer_restart ();
  debounce_timer_start ();

  nrf_drv_gpiote_out_clear (PIN_OUT);
};

void
handle_advertising_timeout ()
{
  advertising_stop ();
  nrf_drv_gpiote_out_set (PIN_OUT);
  nrf_delay_us (200);
  nrf_drv_gpiote_out_clear (PIN_OUT);
}

void
handle_debounce_timeout ()
{
  enable_input ();

  uint8_t contact_ = contact_get ();

  if (contact_ != contact)
    {
      contact = contact_;
      interrupt_count++;
      handle_sensor ();
    }
  else
    {
    }
}

void
app_error_fault_handler (uint32_t id, uint32_t pc, uint32_t info)
{
  unsigned int tmp = id;
  NRF_LOG_DEBUG ("app_error_print():\r\n");
  NRF_LOG_DEBUG ("Fault identifier:  0x%X\r\n", tmp);
  NRF_LOG_DEBUG ("Program counter:   0x%X\r\n", tmp = pc);
  NRF_LOG_DEBUG ("Fault information: 0x%X\r\n", tmp = info);

  switch (id)
    {
    case NRF_FAULT_ID_SDK_ASSERT:
      NRF_LOG_DEBUG ("Line Number: %u\r\n", tmp =
		     ((assert_info_t *) (info))->line_num);
      NRF_LOG_DEBUG ("File Name:   %s\r\n",
		     (uint32_t) ((assert_info_t *) (info))->p_file_name);
      break;

    case NRF_FAULT_ID_SDK_ERROR:
      NRF_LOG_DEBUG ("Line Number: %u\r\n", tmp =
		     ((error_info_t *) (info))->line_num);
      NRF_LOG_DEBUG ("File Name:   %s\r\n",
		     (uint32_t) ((error_info_t *) (info))->p_file_name);
      NRF_LOG_DEBUG ("Error Code:  0x%X\r\n", tmp =
		     ((error_info_t *) (info))->err_code);
      break;
    }
}


int
main (void)
{
  NRF_LOG_INIT (NULL);
  NRF_LOG_DEBUG ("starting\n");

  gpio_init (handle_sensor);

  timer_init (handle_advertising_timeout, handle_debounce_timeout);

  uint8_t contact = contact_get ();
  battery_level = get_current_level ();
  advertising_init (contact, interrupt_count, battery_level);


  fs_init ();

  advertising_start ();

  advertising_timer_start ();

  uint8_t contact2 = contact_get ();
  if (contact2 != contact)
    {
      contact = contact2;
      advertising_data_update (contact2, interrupt_count, battery_level);
    }
  for (;;)
    {
      // if (NRF_LOG_PROCESS () == false){
      sd_app_evt_wait ();
      app_sched_execute ();
      // }
    }
}
