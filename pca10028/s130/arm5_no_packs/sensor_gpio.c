#include "sensor_gpio.h"

uint32_t interrupt_count = 0;
uint32_t contact;

bool ignore_interrupt = false;

void (*sensor_handler) ();


bool
contact_get (void)
{
  return !nrf_gpio_pin_read (PIN_IN);
}

void set_led(bool on){
  #ifdef USE_LEDS
  if(on){
    nrf_drv_gpiote_out_set(PIN_OUT);
  }else{
    nrf_drv_gpiote_out_clear(PIN_OUT);
  }
  #endif
}

void
pin_handler (nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    if(ignore_interrupt){
        return;
    }
  if (pin != PIN_IN)
    {
      return;
    }
  if (!sensor_handler)
    {
      return;
    }
    
    // contact = contact_get();
    contact = (action == NRF_GPIOTE_POLARITY_HITOLO);
    NRF_LOG_DEBUG("contact: %i   action: %i\n", contact, action);
    interrupt_count++;
  sensor_handler ();
}

void
gpio_init (void (*sensor_handler_) ())
{
  ret_code_t err_code;

  err_code = nrf_drv_gpiote_init ();
  APP_ERROR_CHECK (err_code);


#ifdef USE_LEDS
  // output pin

  nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE (false);

  err_code = nrf_drv_gpiote_out_init (PIN_OUT, &out_config);
  nrf_drv_gpiote_out_clear (PIN_OUT);


  // nrf_drv_gpiote_out_config_t out_config2 = GPIOTE_CONFIG_OUT_SIMPLE (false);
  // err_code =   (PIN_OUT2, &out_config2);
#endif

  // input pin
  //Check error code returned
  
  enable_input();
  
  sensor_handler = sensor_handler_;
}

void enable_input(){
    /*
  if(contact){
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI (false);
    in_config.pull = NRF_GPIO_PIN_PULLUP;
    nrf_drv_gpiote_in_init (PIN_IN, &in_config, pin_handler);
  }else{
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO (false);
    in_config.pull = NRF_GPIO_PIN_PULLUP;
    nrf_drv_gpiote_in_init (PIN_IN, &in_config, pin_handler);
  }
    */
  nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE (false);
  in_config.pull = NRF_GPIO_PIN_PULLUP;
  nrf_drv_gpiote_in_init (PIN_IN, &in_config, pin_handler);
  ignore_interrupt = true;
  nrf_drv_gpiote_in_event_enable (PIN_IN, true);
  ignore_interrupt = false;
}

void disable_input(){
  nrf_drv_gpiote_in_event_disable (PIN_IN);
  nrf_drv_gpiote_in_uninit(PIN_IN);
}
