#ifndef SENSOR_TIMER_H
#define SENSOR_TIMER_H

#include "app_timer.h"
#include "app_timer_appsh.h"
#include "app_scheduler.h"

#define APP_TIMER_PRESCALER             0                                 /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         6                                 /**< Size of timer operation queues. */
#define APP_SCHEDULER_QUEUE_SIZE 				6

#define ADVERTISING_TIMER_TIMEOUT 1500
#define DEBOUNCE_TIMER_TIMEOUT 1000

void advertising_timer_start(void);
void advertising_timer_stop(void);
void timer_init(void (*)(), void (*)());
void advertising_timer_restart(void);
void debounce_timer_start(void);

#endif
