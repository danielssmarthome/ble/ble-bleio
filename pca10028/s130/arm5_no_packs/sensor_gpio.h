#ifndef SENSOR_GPIO_H
#define SENSOR_GPIO_H

#include "nrf_drv_gpiote.h"
#include "nrf_log.h"

#define BATTERY_VOLTAGE_MAX 3000.0
#define BATTERY_VOLTAGE_MIN 1700.0

#define PIN_IN 28
#define PIN_OUT 29


#define USE_LEDS

bool contact_get(void);
void pin_handler(nrf_drv_gpiote_pin_t, nrf_gpiote_polarity_t);
void gpio_init(void (*sensor_handler)());
uint8_t battery_level_get(void);
void set_led(bool on);
void enable_input(void);
void disable_input(void);

extern uint32_t interrupt_count;
extern uint32_t contact;

#endif
