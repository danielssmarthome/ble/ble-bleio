#include "sensor_timer.h"


APP_TIMER_DEF(advertising_timer);
APP_TIMER_DEF(debounce_timer);

void (*advertising_timeout_handler_func)();
void (*debounce_timeout_handler_func)();

void advertising_timeout_handler(void * context){
    advertising_timeout_handler_func();
}

void debounce_timeout_handler(void * context){
    debounce_timeout_handler_func();
}

void advertising_timer_start(){
	uint32_t err = app_timer_start(advertising_timer, APP_TIMER_TICKS(ADVERTISING_TIMER_TIMEOUT, 0), NULL);
    APP_ERROR_CHECK(err);
}

void advertising_timer_stop(){
	app_timer_stop(advertising_timer);
}

void debounce_timer_start(){
	uint32_t err = app_timer_start(debounce_timer, APP_TIMER_TICKS(DEBOUNCE_TIMER_TIMEOUT, 0), NULL);
    APP_ERROR_CHECK(err);
}

void advertising_timer_restart(){
  	advertising_timer_stop();
    advertising_timer_start();
};

void timer_init(void (*advertising_timeout_handler_func_)(), void (*debounce_timeout_handler_func_)()){
    advertising_timeout_handler_func = advertising_timeout_handler_func_;
    debounce_timeout_handler_func = debounce_timeout_handler_func_;
	
	  APP_SCHED_INIT(sizeof(app_timer_event_t), APP_SCHEDULER_QUEUE_SIZE);
	
	  APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, true);
    
    uint32_t err_code = app_timer_create(&advertising_timer, 
                                         APP_TIMER_MODE_SINGLE_SHOT, 
                                         advertising_timeout_handler);
  
    err_code = app_timer_create(&debounce_timer, 
                                         APP_TIMER_MODE_SINGLE_SHOT, 
                                         debounce_timeout_handler);
}
