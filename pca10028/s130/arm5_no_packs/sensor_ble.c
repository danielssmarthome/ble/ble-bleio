#include "sensor_ble.h"

ble_gap_adv_params_t m_adv_params;
ble_advdata_t advdata;

ble_dfu_t dfu;
ble_bas_t m_bas;

bool advertising_started = false;
static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */

static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("connected\r\n");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break; // BLE_GAP_EVT_CONNECTED

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("disconnected\r\n");
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break; // BLE_GAP_EVT_DISCONNECTED

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
                                                   BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP,
                                                   NULL,
                                                   NULL);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GAP_EVT_SEC_PARAMS_REQUEST

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GATTS_EVT_SYS_ATTR_MISSING

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.\r\n");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GATTC_EVT_TIMEOUT

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.\r\n");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GATTS_EVT_TIMEOUT

        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            break; // BLE_EVT_USER_MEM_REQUEST

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST

#if (NRF_SD_BLE_API_VERSION == 3)
        case BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST:
            err_code = sd_ble_gatts_exchange_mtu_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                       NRF_BLE_MAX_MTU_SIZE);
            APP_ERROR_CHECK(err_code);
            break; // BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST
#endif

        default:
            // No implementation needed.
            break;
    }
}

static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    on_ble_evt(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_dfu_on_ble_evt(&dfu, p_ble_evt);
    ble_bas_on_ble_evt(&m_bas, p_ble_evt);
}


static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    NRF_LOG_DEBUG("init: %i\n", err_code);
    APP_ERROR_CHECK(err_code);
}

void
advertising_stack_init (void)
{
  uint32_t err_code;

  nrf_clock_lf_cfg_t clock_lf_cfg = {
    .source = NRF_CLOCK_LF_SRC_XTAL,
    .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM,
  };

  // bool running = 

  // Initialize the SoftDevice handler module.
  SOFTDEVICE_HANDLER_INIT (&clock_lf_cfg, NULL);

  ble_enable_params_t ble_enable_params;
  err_code = softdevice_enable_get_default_config (CENTRAL_LINK_COUNT,
						   PERIPHERAL_LINK_COUNT,
						   &ble_enable_params);
  APP_ERROR_CHECK (err_code);

  //Check the ram settings against the used number of links
  CHECK_RAM_START_ADDR (CENTRAL_LINK_COUNT, PERIPHERAL_LINK_COUNT);

  ble_enable_params.common_enable_params.vs_uuid_count = 1;

  // Enable BLE stack.
  err_code = softdevice_enable (&ble_enable_params);
  APP_ERROR_CHECK (err_code);



  ble_gap_conn_sec_mode_t sec_mode;
  BLE_GAP_CONN_SEC_MODE_SET_OPEN (&sec_mode);
  uint8_t *name = (uint8_t *) DEVICE_NAME;
  sd_ble_gap_device_name_set (&sec_mode, name, strlen (DEVICE_NAME));


  // Subscribe for BLE events.
  err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
  APP_ERROR_CHECK(err_code);

  // Initialize advertising parameters (used when starting advertising).
  /*memset (&m_adv_params, 0, sizeof (m_adv_params));

     m_adv_params.type = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
     m_adv_params.p_peer_addr = NULL;   // Undirected advertisement.
     m_adv_params.fp = BLE_GAP_ADV_FP_ANY;
     m_adv_params.interval = NON_CONNECTABLE_ADV_INTERVAL;
     m_adv_params.timeout = ADVERTISING_TIMEOUT; */
     
     
  gap_params_init ();

  services_init ();
  
  conn_params_init();
}


void
advertising_data_init ()
{
  uint32_t err;
  memset (&advdata, 0, sizeof (advdata));

  int8_t power = 4;

  advdata.name_type = BLE_ADVDATA_FULL_NAME;
  //advdata.include_appearance = true;
  advdata.flags = BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED;
  advdata.p_tx_power_level = &power;

  err = sd_ble_gap_tx_power_set (power);
  APP_ERROR_CHECK (err);
  err = sd_ble_gap_appearance_set (BLE_APPEARANCE_GENERIC_REMOTE_CONTROL);
  APP_ERROR_CHECK (err);
}

void
advertising_init (uint8_t contact, uint32_t count, uint8_t battery_level)
{
  advertising_stack_init ();
  advertising_data_init ();
  advertising_data_update (contact, count, battery_level);
}

void
advertising_data_update (uint8_t contact, uint32_t count,
			 uint8_t battery_level)
{
  uint8_t size =
    sizeof (contact) + sizeof (count) + sizeof (battery_level) + 6;
  uint8_t data[size];
  data[0] = size - 1;
  data[1] = data[2] = data[3] = 0xFF;
  data[4] = BOARD_TYPE;
  data[5] = PROTOCOL_VERSION;
  data[6] = contact;
  memcpy (data + 7, &count, sizeof (count));
  data[size - 1] = battery_level;

  uint32_t err = sd_ble_gap_adv_data_set (data, size, NULL, 0);
  APP_ERROR_CHECK (err);
}



/**@brief Function for starting advertising.
 */
void
advertising_start (void)
{
  if (advertising_started)
    {
      return;
    }
  uint32_t err_code;
  ble_gap_adv_params_t adv_params;

  // Start advertising
  memset (&adv_params, 0, sizeof (adv_params));

  adv_params.type = BLE_GAP_ADV_TYPE_ADV_IND;
  adv_params.p_peer_addr = NULL;
  adv_params.fp = BLE_GAP_ADV_FP_ANY;
  adv_params.interval = APP_ADV_INTERVAL;
  adv_params.timeout = BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED;

  err_code = sd_ble_gap_adv_start (&adv_params);
    
  APP_ERROR_CHECK (err_code);
  advertising_started = true;
}



/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


uint32_t bas_init(){
    ble_bas_init_t bas_init = {
        .evt_handler = NULL,
        .support_notification = false,
        .p_report_ref = NULL,
        .initial_batt_level = 255
    };

    // Here the sec level for the Battery Service can be changed/increased.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init.battery_level_char_attr_md.write_perm);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_report_read_perm);

    return ble_bas_init(&m_bas, &bas_init);
}


uint32_t dfu_init(){
    ble_dfu_init_t init = {
        .evt_handler = NULL
    };
    return ble_dfu_init(&dfu, &init);
}

/**@brief Function for initializing services that will be used by the application.
 */
void services_init(void)
{
    uint32_t   err_code;
    
    err_code = dfu_init();
    APP_ERROR_CHECK(err_code);
	
    err_code = bas_init();
    APP_ERROR_CHECK(err_code);
}

void
advertising_stop ()
{
  if (!advertising_started)
    {
      return;
    }
  uint32_t err = sd_ble_gap_adv_stop ();
  APP_ERROR_CHECK (err);
  advertising_started = false;
}
