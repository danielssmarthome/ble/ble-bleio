ADB_TARGET = 192.168.0.116:5555

APPLICATION_HEX = pca10028/s130/arm5_no_packs/_build/*.hex

KEY_FILE = ../../../../private.pem

OUT_ZIP = update.zip

.PHONY: applicaiton_zip push

all: applicaiton_zip push

applicaiton_zip:
	ls -lh pca10028/s130/arm5_no_packs/_build/*.hex
	nrfutil pkg generate --application $(APPLICATION_HEX) --debug-mode $(OUT_ZIP) --key-file $(KEY_FILE)
	
push:
	adb connect $(ADB_TARGET)
	adb push update.zip /sdcard/